#!/bin/sh

set -e

logexec() {
    echo "$@"
    $@
}

rm -f lib/glob.h
ln -s glob.in.h lib/glob.h

FILES="ar arscan commands default dir expand file function getopt getopt1 guile implicit job load loadapi main misc posixos output read remake rule signame strcache variable version vpath hash remote-stub ../lib/glob"

cd src

OBJS=""
for f in $FILES ; do
    logexec tcc -DLOCALEDIR=\"/usr/local/share/locale\" -DLIBDIR=\"/usr/local/lib\" -DINCLUDEDIR=\"/usr/local/include\" -DMULTIARCH_DIRS="" -DHAVE_CONFIG_H -I. -I../lib -g -O2 -c -o $f.o $f.c
    OBJS="$OBJS $f.o"
done

logexec tcc -g -O2 -o make $OBJS -ldl
